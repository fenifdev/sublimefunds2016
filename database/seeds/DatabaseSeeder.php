<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        DB::table('transactions_types')->insert(
          array(
              array(
                'name' => 'Deposit'
              ),
              array(
                'name' => 'Withdrawal'
              ),
              array(
                'name' => 'Investment'
              ),
              array(
                'name' => 'Return'
              ),
              array(
                'name' => 'Investment End'
              ),
          )
        );

        DB::table('plans')->insert(
          array(
              array(
                'name' => 'Cobre',
                'tnm' => '4',
                'months' => '1'
              ),
              array(
                'name' => 'Bronze',
                'tnm' => '5',
                'months' => '3'
              ),
              array(
                'name' => 'Plata',
                'tnm' => '6',
                'months' => '6'
              ),
              array(
                'name' => 'Oro',
                'tnm' => '7',
                'months' => '9'
              ),
              array(
                'name' => 'Diamante',
                'tnm' => '8',
                'months' => '12'
              ),
          )
        );

        Model::reguard();
    }
}
