<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentsReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investments_returns', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('investment_id')->unsigned();
          $table->decimal('amount', 10, 2);
          $table->date('date_end');
          $table->boolean('status');
          $table->timestamps();
          $table->foreign('investment_id')->references('id')->on('investments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('investments_returns');
    }
}
