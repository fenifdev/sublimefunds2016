@extends('layouts.panel')
@section('content')
  <div class="container">
    <h3>Retirar Fondos</h3>
    <div class="barrita_basica_1"></div>
    <br/>
    @if(count($errors)>0)
    <div class="alert alert-danger text-center">
      <ul>
        @foreach($errors->all() as $error)
        <li>{!!$error!!}</li>
        @endforeach
      </ul>
    </div>
    @endif
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="alert alert-info text-center">
    	<strong>Recuerde</strong>
    	el monto minimo para retirar es de 100usd
    </div>
    {!!Form::open(['url' => 'panel/create_withdrawal', 'method'=>'POST', 'class'=>'form-horizontal', 'role'=>'form'])!!}
    	<div class="form-group">
    		<label class="col-sm-3 control-label">Saldo Disponible</label>
    		<div class="col-sm-9">
    			${!!$user->wallet->balance_available!!} USD
    		</div>
    	</div>
    	<div class="form-group">
    		<label for="exampleInputCantidad" class="col-sm-3 control-label">Monto a Retirar</label>
    		<div class="col-sm-9">
          {!!Form::number('amount',null,['class'=>'form-control','placeholder'=>'Cantidad'])!!}
    		</div>
    	</div>
    	<div class="text-right">
        {!!Form::submit('Retirar Fondos',['class'=>'btn btn-color_propio'])!!}
    	</div>
    {!!Form::close()!!}
    <br/>
  </div>
@endsection
