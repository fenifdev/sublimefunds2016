@extends('layouts.front')
@section('content')
<div class="container">
	<h3>404 Page not found</h3>
</div>
<div class="barrita_basica_1"></div>
<br/>
<div class="container">
	<h1 class="text-center" style="font-size:10em;">404</h1>
	<h4 class="text-center">Error 404! Sorry, the page you requested was not found.</h4>
	<br/>
	<div class="text-center">
		<a href="{!!url('/')!!}">
			<button class="btn btn-color_propio btn-lg">Back To Home</button>
		</a>
	</div>
</div>
@endsection
