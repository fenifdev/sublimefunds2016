<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SublimeFunds</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="icon" type="image/x-icon" href="{!!URL::asset('img/favicon.ico')!!}" />
        {!!Html::style('css/main.css')!!}
        {!!Html::style('css/bootstrap.min.css')!!}
        {!!Html::style('css/sublime.css')!!}
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <!--<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">-->
        <link rel="stylesheet" href="//cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css">
        <style>
        /* Sticky footer styles
        -------------------------------------------------- */
        html {
          position: relative;
          min-height: 100%;
        }
        body {
          /* Margin bottom by footer height */
          margin-bottom: 80px;
        }
        .footer {
          position: absolute;
          bottom: 0;
          width: 100%;
          /* Set the fixed height of the footer here */
          height: 70px;
          height: auto;
          background-color: #f5f5f5;
        }
        </style>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
          <div class="container-fluid">
      	    <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="{!!url('/panel')!!}"><img src="{!!URL::asset('img/logo.png')!!}" width='150px'></a>
              </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                      <li>{!!link_to_route('front.index', $title = 'Inicio', $parameters = [], $attributes = [])!!}</li>
                      <li>{!!link_to_action('UserController@dashboard', $title = 'Panel', $parameters = [], $attributes = [])!!}</li>
                      @if(Auth::user()->role=='admin')
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li>{!!link_to_action('UserController@index', $title = 'Usuarios', $attributes = [], $secure = null)!!}</li>
                          <li role="separator" class="divider"></li>
                          <li>{!!link_to_action('InvestmentController@indexAdmin', $title = 'Inversiones', $attributes = [], $secure = null)!!}</li>
                          <li role="separator" class="divider"></li>
                          <li>{!!link_to_action('TransactionController@indexAdmin', $title = 'Transacciones', $attributes = [], $secure = null)!!}</li>
                          <li role="separator" class="divider"></li>
                          <li>Ver Retiros</li>
                        </ul>
                      </li>
                      @endif
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Operaciones <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li>{!!link_to_action('DepositController@create', $title = 'Depositar', $attributes = [], $secure = null)!!}</li>
                          <li>{!!link_to_action('WithdrawalController@create', $title = 'Retirar', $attributes = [], $secure = null)!!}</li>
                          <li role="separator" class="divider"></li>
                          <li>{!!link_to_action('InvestmentController@create', $title = 'Invertir', $attributes = [], $secure = null)!!}</li>
                          <li>{!!link_to_action('InvestmentController@index', $title = 'Inversiones', $attributes = [], $secure = null)!!}</li>
                          <li role="separator" class="divider"></li>
                          <li>{!!link_to_action('TransactionController@index', $title = 'Historial', $attributes = [], $secure = null)!!}</li>
                        </ul>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{!!Auth::user()->name!!} <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="#">Editar Perfil</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="{!!action('Auth\AuthController@getLogout')!!}">Logout</a></li>
                        </ul>
                      </li>
                  </ul>
              </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
      </nav>
        @yield('content')
        <br/>
        <br/>
        <div class="container-fluid footer" id="footer">
            <div class="container">
                <div class="row">
                  <div class="col-md-12 col-lg-12">
                    <div class="text-center">
                      Copyright © 2013 - 2014 Sublime Funds LTD. All Right Reserved.
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-12">
                    <div class="col-xs-12 col-md-6 col-lg-6 text-right">
                      {!!link_to_route('front.PoliticaDePrivacidad', $title = 'Politica de Privacidad', $parameters = [], $attributes = [])!!}
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6 text-left">
                      {!!link_to_route('front.TerminosYCondiciones', $title = 'Terminos y Condiciones', $parameters = [], $attributes = [])!!}
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <!--{!!Html::script('js/bootstrap.min.js')!!}-->
        <!-- DataTables -->
        <!--<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>-->
        <script src="//cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
        <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url('admin/user/datatablesData') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
            $('#transactions-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url('panel/transactionDatatablesData') !!}',
                columns: [
                    { data: 'amount', name: 'amount' },
                    { data: 'TransactionType', name: 'transaction_type_id' },
                    { data: 'created_at', name: 'created_at' }
                ]
            });
            $('#transactionsAdmin-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url('admin/transactionDatatablesData') !!}',
                columns: [
                    { data: 'amount', name: 'amount' },
                    { data: 'TransactionType', name: 'transaction_type_id' },
                    { data: 'created_at', name: 'created_at' }
                ]
            });
            $('#investments-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url('panel/investmentDatatablesData') !!}',
                columns: [
                    { data: 'amount', name: 'amount' },
                    { data: 'plan_name', name: 'plan_id' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'date_start', name: 'date_start' },
                    { data: 'date_end', name: 'date_end' }
                ]
            });
            $('#investmentsAdmin-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url('admin/investmentDatatablesData') !!}',
                columns: [
                    { data: 'amount', name: 'amount' },
                    { data: 'plan_name', name: 'plan_id' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'date_start', name: 'date_start' },
                    { data: 'date_end', name: 'date_end' }
                ]
            });
        });
        </script>
    </body>
</html>
