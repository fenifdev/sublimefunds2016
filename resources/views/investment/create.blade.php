@extends('layouts.panel')
@section('content')
  <div class="container">
    <h3>Invertir</h3>
    <div class="barrita_basica_1"></div>
    <br/>
    @if(count($errors)>0)
    <div class="alert alert-danger text-center">
      <ul>
        @foreach($errors->all() as $error)
        <li>{!!$error!!}</li>
        @endforeach
      </ul>
    </div>
    @endif
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="alert alert-info text-center">
    	<strong>Importante:</strong> el monto minimo de inversión es de 100 USD.
    </div>
    {!!Form::open(['url' => 'panel/create_investment', 'method'=>'POST', 'class'=>'form-horizontal', 'role'=>'form'])!!}
    	<div class="form-group">
    		<label for="exampleInputMonto" class="col-sm-2 control-label">Monto Disponible:</label>
    		<div class="col-sm-10">
    			${!!$user->wallet->balance_available!!} USD
    		</div>
    	</div>
    	<div class="form-group">
    		<label for="exampleInputMonto" class="col-sm-2 control-label">Monto:</label>
    		<div class="col-sm-10">
          {!!Form::number('amount',null,['class'=>'form-control','placeholder'=>'Monto'])!!}
    		</div>
    	</div>
    	<div class="form-group">
    		<label for="exampleInputPlan" class="col-sm-2 control-label">Plan:</label>
    		<div class="col-sm-10">
          {!!Form::select('plan_id', $data_plans, null, ['class'=>'form-control','placeholder' => 'Seleccione un plan...'])!!}
    		</div>
    	</div>
    	<div class="text-right">
        {!!Form::submit('Invertir',['class'=>'btn btn-color_propio'])!!}
    	</div>
    {!!Form::close()!!}
    <br/>
  </div>
@endsection
