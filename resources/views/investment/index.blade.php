@extends('layouts.panel')
@section('content')
  <div class="container">
    <h3>Ver Inversiones</h3>
    <div class="barrita_basica_1"></div>
    <br/>
    @if(count($user->investments)>0)
        <!--<table class="table">
          <thead>
            <tr>
              <th>Monto</th>
              <th>Plan</th>
              <th>Fecha</th>
              <th>Inicio</th>
              <th>Fin</th>
            </tr>
          </thead>
          <tbody>
          @foreach($user->investments as $investment)
              <tr>
                  <td>${!!number_format($investment->amount,2)!!} USD</td>
                  <td>{!!$investment->plan->name!!}</td>
                  <td>{!!$investment->created_at->diffForHumans()!!}</td>
                  <td>{!!$investment->date_start!!}</td>
                  <td>{!!$investment->date_end!!}</td>
                </tr>
          @endforeach
          </tbody>
        </table>-->
        <table class="table table-bordered" id="investments-table">
          <thead>
              <tr>
                  <th>Monto</th>
                  <th>Plan</th>
                  <th>Fecha</th>
                  <th>Inicio</th>
                  <th>Fin</th>
              </tr>
          </thead>
        </table>
    @else
      <div class="alert alert-warning text-center">Todavía no ha realizado inversiones.</div>
    @endif
    <br/>
  </div>
@endsection
