@extends('layouts.panel')
@section('content')
  <div class="container">
    <h3>Ver Inversiones</h3>
    <div class="barrita_basica_1"></div>
    <br/>
    @if(count($investments)>0)
        <table class="table table-bordered" id="investmentsAdmin-table">
          <thead>
              <tr>
                  <th>Monto</th>
                  <th>Plan</th>
                  <th>Fecha</th>
                  <th>Inicio</th>
                  <th>Fin</th>
              </tr>
          </thead>
        </table>
    @else
      <div class="alert alert-warning text-center">Todavía no ha realizado inversiones.</div>
    @endif
    <br/>
  </div>
@endsection
