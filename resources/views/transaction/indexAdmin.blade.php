@extends('layouts.panel')
@section('content')
  <div class="container">
    <h3>Ver Transacciones</h3>
    <div class="barrita_basica_1"></div>
    <br/>
    @if(count($transactions)>0)
        <table class="table table-bordered" id="transactionsAdmin-table">
          <thead>
              <tr>
                  <th>Monto</th>
                  <th>Tipo</th>
                  <th>Fecha</th>
              </tr>
          </thead>
        </table>
    @else
      <div class="alert alert-warning text-center">Todavía no ha realizado transacciones.</div>
    @endif
    <br/>
  </div>
@endsection
