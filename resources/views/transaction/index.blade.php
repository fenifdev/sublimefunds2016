@extends('layouts.panel')
@section('content')
  <div class="container">
    <h3>Ver Transacciones</h3>
    <div class="barrita_basica_1"></div>
    <br/>
    @if(count($user->transactions)>0)
        <!--
        <table class="table">
          <thead>
            <tr>
              <th>Monto</th>
              <th>Tipo</th>
              <th>Fecha</th>
            </tr>
          </thead>
          <tbody>
          @foreach($user->transactions as $transaction)
              <tr>
                  <td>${!!number_format($transaction->amount,2)!!} USD</td>
                  <td>{!!$transaction->TransactionType->name!!}</td>
                  <td>{!!$transaction->created_at!!}</td>
                </tr>
          @endforeach
          </tbody>
        </table>-->
        <table class="table table-bordered" id="transactions-table">
          <thead>
              <tr>
                  <th>Monto</th>
                  <th>Tipo</th>
                  <th>Fecha</th>
              </tr>
          </thead>
        </table>
    @else
      <div class="alert alert-warning text-center">Todavía no ha realizado transacciones.</div>
    @endif
    <br/>
  </div>
@endsection
