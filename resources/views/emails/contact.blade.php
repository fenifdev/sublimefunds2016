<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
</head>
<body>
<p>Name: {!!$name!!}</p>
<p>Email: {!!$email!!}</p>
<p>Subject: {!!$subject!!}</p>
<p>Message: {!!$messages!!}</p>
</body>
</html>
