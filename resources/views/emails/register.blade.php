<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
</head>
<body>
  <table style="background:#ddd" border="0" cellpadding="0" cellspacing="0" width="100%">
  	<tbody>
  		<tr>
  			<td style='padding:10px'>
  				<table style="background:#fff;border:0;border:1px solid #ccc;padding:10px;" align="center" border="0" cellpadding="0" cellspacing="0" width="670">
  					<tbody>
  						<tr>
  							<td style="font-size:14px;color:#666;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:bold;">
  								{!!$user->name!!}
  							</td>
  						</tr>
  						<tr>
  							<td style="font-size:14px;color:#666;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;">
  								Por favor, confirma tu cuenta de Sublime Funds
  							</td>
  						</tr>
  						<tr>
  							<td style="font-size:14px;padding:10px 0px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;">Confirmar tu cuenta te dará acceso completo a Sublime Funds y todas las notificaciones futuras serán enviadas a esta dirección de correo electrónico.
  							</td>
  						</tr>
  						<tr>
  							<td>
  								<a style="background-color:#2eafbb;border-radius:5px;border:1px solid #178E9D;color:white;text-decoration:none;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:bold;font-size:14px;padding:5px 0px 5px 5px;display:block;width:200px;" href='{!!$user->link!!}'>
  									Confirmar tu cuenta ahora
  								</a>
  							</td>
  						</tr>
  						<tr>
  							<td style="font-size:14px;padding:10px 0px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;">
  								O haz clic en el siguiente enlace:
  							</td>
  						</tr>
  						<tr>
  							<td style="padding:10px 0px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;">
  								<a style='color:#2eafbb;text-decoration:none;' href='{!!$user->link!!}'>
  									{!!$user->link!!}
  								</a>
  							</td>
  						</tr>
  						<tr>
  							<td style="background-color:#eee;padding:10px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;">
  								Sublime Funds LTD.
  							</td>
  						</tr>
  					</tbody>
  				</table>
  			</td>
  		</tr>
  	</tbody>
  </table>
</body>
</html>
