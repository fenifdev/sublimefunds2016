@extends('layouts.panel')
@section('content')
  <div class="container">
    <h3>Depositar</h3>
    <div class="barrita_basica_1"></div>
    <br/>
    @if(count($errors)>0)
    <div class="alert alert-danger text-center">
      <ul>
        @foreach($errors->all() as $error)
        <li>{!!$error!!}</li>
        @endforeach
      </ul>
    </div>
    @endif
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('paypal_error'))
        <div class="alert alert-danger text-center">
            {{ session('paypal_error') }}
        </div>
    @endif
    <div class="alert alert-info text-center">
    	Depósite fondos y empieze a invertir
    </div>
    <div class="text-center">
      {!!Form::open(['url' => 'panel/create_deposit', 'method'=>'POST', 'class'=>'form-horizontal', 'role'=>'form'])!!}
        <div class="form-group">
          <label for="exampleInputPassword" class="control-label col-lg-4 col-md-4">Contraseña:</label>
          <div class="col-lg-8 col-md-8">
            {!!Form::number('amount',null,['class'=>'form-control','placeholder'=>'Cantidad'])!!}
          </div>
        </div>
        {!!Form::submit('Depositar',['class'=>'btn btn-color_propio'])!!}
      {!!Form::close()!!}
    </div>
  </div>
@endsection
