@extends('layouts.panel')
@section('content')
  <div class="container">
    <h3>Usuarios</h3>
    <div class="barrita_basica_1"></div>
    <br/>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
  </div>
@endsection
