@extends('layouts.panel')
@section('content')
  <div class="container">
    <h3>Editar Usuario</h3>
    <div class="barrita_basica_1"></div>
    <br/>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if(count($errors)>0)
    <div class="alert alert-danger text-center">
      <ul>
        @foreach($errors->all() as $error)
        <li>{!!$error!!}</li>
        @endforeach
      </ul>
    </div>
    @endif
    {!!Form::model($user,['route' => ['admin.user.update',$user->id], 'method'=>'PUT'])!!}
        <div class="form-group">
          {!!Form::label('Nombre')!!}
          {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Ingresa el nombre'])!!}
        </div>
        <div class="form-group">
          {!!Form::label('Email')!!}
          {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'Ingresa el email'])!!}
        </div>
        <div class="form-group">
          @if(Auth::user()->role=='admin')
          {!!Form::label('Role')!!}
          {!!Form::select('role', $data_role, null, ['class'=>'form-control','placeholder' => 'Seleccione un role...'])!!}
          @else
          {!!Form::select('role', $data_role, null, ['style'=>'display:none;'])!!}
          @endif
        </div>
        <div class="form-group">
          {!!Form::label('Contraseña')!!}
          {!!Form::password('password',['class'=>'form-control','placeholder'=>'Ingresa el password'])!!}
        </div>
        {!!Form::submit('Editar',['class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

  </div>
@endsection
