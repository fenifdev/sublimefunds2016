@extends('layouts.panel')
@section('content')
  <div class="container">
    <h3>Cuenta</h3>
    <div class="barrita_basica_1"></div>
    <br/>
    <ul class="list-group">
      <li class="list-group-item">
        <span class="badge">${!!$user->wallet->balance!!} USD</span>
        Saldo
      </li>
      <li class="list-group-item">
        <span class="badge">${!!$user->wallet->balance_available!!} USD</span>
        Saldo Disponible
      </li>
      <li class="list-group-item">
        <span class="badge">${!!$user->wallet->earning_income!!} USD</span>
        Saldo Pendiente de ingreso
      </li>
      <li class="list-group-item">
        <span class="badge">${!!$user->wallet->pending_withdrawal!!} USD</span>
        Saldo Pendiente de retiro
      </li>
      <li class="list-group-item">
        <span class="badge">${!!$user->wallet->invested!!} USD</span>
        Saldo Invertido
      </li>
    </ul>
  </div>
@endsection
