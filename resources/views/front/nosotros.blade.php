@extends('layouts.front')
@section('content')
  <div class="container">
  	<h3>Nosotros</h3>
  </div>
  <div class="barrita_basica_1"></div>
  <br/>
  <div class="container">
  	<img src="{!!URL::asset('img/nosotros.jpg')!!}" class="img-responsive pull-left img-nosotros">
  	<p><strong>El Equipo:</strong> <span class="color_propio_2">Sublime Funds LTD tiene como Actividad Principal es el “Manejo de Capital e Inversiones Varias”, En su mayoría el capital es utilizado para monetizar sitios web´s, comercio online.</span></p>
  	<p><strong>Misión:</strong> <span class="color_propio_2">Como inversores tenemos el deber de actuar para el bien de nuestros usuarios, brindando los mejores servicios de calidad - seguridad que existen y estén a nuestro alcance en el mercado actual. Logrando de esta manera una relación cordial, cercana y segura con nuestros usuarios.</span></p>
  	<p><strong>Visión:</strong> <span class="color_propio_2">El ámbito financiero mundial tiene sus altos y bajos. Por ese motivo nuestra visión es brindar todos los servicios al momento que el usuario decide realizar la inversión, desarrollando y actualizando las aplicaciones que utilizamos. Creciendo de forma estable y responsable.</span></p>
  	<p><strong>Valores:</strong> <span class="color_propio_2">El manejo de capitales es delicado, por eso contamos con planes de inversión,  infraestructura con personal capacitado. Invertimos de forma segura, evitando poner en riesgo el capital de nuestros usuarios y asegurando las ganancias para ambas partes.</span></p>
  	<p><strong>Historia:</strong> <span class="color_propio_2">La creación de Sublime Funds LTD fue el resultado de varias experiencias personales por parte de los integrantes, procedentes de diversos ámbitos económicos, con pensamientos distintos pero un objetivo común “Inversión Segura”, esto llevo a la creación del equipo, un plan de negocios y lo más importante “poder compartir”, no fue tarea fácil, en el camino se aprendió de los aciertos y errores tanto personales como grupales, varios meses de preparación muchos cambios de ideas. Todo esto derivo en lo que es hoy Sublime Funds LTD, un grupo dedicado íntegramente en brindar un servicio de inversión simple y básico, pero escasos en este mundo moderno, el equipo de Sublime Funds LTD está orgulloso de lo alcanzado!.</span></p>
  </div>
@endsection
