@extends('layouts.front')
@section('content')
  <div class="container">
    <h3>Preguntas Frecuentes</h3>
  </div>
  <div class="barrita_basica_1"></div>
  <br/>
  <div class="container">
    <div class="panel-group" id="accordion">
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Cómo es tratada la información personal del Usuario?</strong>
        </a>
        <div id="collapse1" class="collapse in">
          <div class="color_propio_2">
            Para Sublime Funds LTD, la privacidad de los clientes es de suma importancia. Utilizamos la información personal de nuestros usuarios con el solo fin de ayudar a mejorar nuestros servicios y asegurar que su experiencia con nosotros sea placentera.
          </div>
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
           <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
           <strong>¿Qué garantía ofrece Sublime Funds a sus usuarios?</strong>
        </a>
      </div>
      <div id="collapse2" class="collapse">
        <div class="color_propio_2">
          Nuestros usuarios pueden disfrutar de una garantía total, aseguramos el ciento por ciento del capital invertido. También garantizamos los % de ganancias que nuestra plataforma brinda.
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Puede un usuario crear múltiples cuentas?</strong>
        </a>
      </div>
      <div id="collapse3" class="collapse">
        <div class="color_propio_2">
          No, un usuario se le permite abrir sólo una cuenta.
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Cuáles son los riesgos potenciales relacionados con Sublime Funds?</strong>
        </a>
      </div>
      <div id="collapse4" class="collapse">
        <div class="color_propio_2">
          Hasta la fecha hemos cumplido con todo lo acordado, ninguna de las dos partes corre riesgo alguno. La inversión se lleva a cabo cuando se puede brindar el servicio completamente, de lo contrario, se avisara con tiempo que el cupo está llegando a su límite. Si no podemos hacer trabajar el dinero invertido, no se permite que ingresen nuevas inversiones; de esta forma las dos partes están resguardadas por la transparencia y la sinceridad.
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Cuál es el monto mínimo de inversión?</strong>
        </a>
      </div>
      <div id="collapse5" class="collapse">
        <div class="color_propio_2">
          El mínimo es de U$D 50 (cincuenta dólares americanos).
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Qué medios posee el usuario para realizar su depósito?</strong>
        </a>
      </div>
      <div id="collapse6" class="collapse">
        <div class="color_propio_2">
          Nosotros Trabajamos con PayPal desde el inicio. Creemos que es la plataforma de pago online mas confiable hasta el momento en el mercado.
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Puede un usuario realizar varias inversiones?</strong>
        </a>
      </div>
      <div id="collapse7" class="collapse">
        <div class="color_propio_2">
          Sí, un usuario puede hacer inversiones adicionales como y cuando quiera. Nuestro sistema maneja todas las operaciones por separado.
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Puede el usuario cancelar o cambiar su inversión una vez realizada?</strong>
        </a>
      </div>
      <div id="collapse8" class="collapse">
        <div class="color_propio_2">
          No, el usuario debe esperar el tiempo seleccionado para esa inversión.
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Cuáles son los tipos de interés?</strong>
        </a>
      </div>
      <div id="collapse9" class="collapse">
        <div class="color_propio_2">
          Las tasas de interés varían dependiendo del tiempo elegido por el usuario. Estas tasas varian desde 4% hasta 8% mensual (TNM).
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Cuando las inversiones comienzan a producir interés?</strong>
        </a>
      </div>
      <div id="collapse10" class="collapse">
        <div class="color_propio_2">
          Desde el reinicio del horario del servidor (Día siguiente).
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Qué información brindan al usuario para poder confiar en su Sublime Funds?</strong>
        </a>
      </div>
      <div id="collapse11" class="collapse">
        <div class="color_propio_2">
          Estamos en línea desde el 1° de Noviembre de 2013. Contamos con una base solida de usuarios y ninguna disputa realizada hacia nosotros. Cumplimos con los tiempos pactados y ofrecemos únicamente lo que podemos cumplir.
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Cuál es el importe mínimo para solicitar retiro de fondos?</strong>
        </a>
      </div>
      <div id="collapse12" class="collapse">
        <div class="color_propio_2">
          El saldo mínimo es de U$D 20.
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Cuánto tiempo se tarda en procesar una solicitud de retiro?</strong>
        </a>
      </div>
      <div id="collapse13" class="collapse">
        <div class="color_propio_2">
          El tiempo mínimo es de 1 hs y el máximo de 24 hs corridas
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse14">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿Se puede ganar comisión de referencia?</strong>
        </a>
      </div>
      <div id="collapse14" class="collapse">
        <div class="color_propio_2">
          NO. Sublime Funds no posee planes, bonos ni esquema de referencias.
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
      <div class="contenedor_pregunta_faq">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse15">
          <button class="btn btn-color_propio btn-xs"><i class="fa fa-question"></i></button>
          <strong>¿El usuario puede contactar con el soporte las 24 hs?</strong>
        </a>
      </div>
      <div id="collapse15" class="collapse">
        <div class="color_propio_2">
          Si. Colocamos a disposición 3 formas de comunicación: Completando el formulario de Contacto, Utilizando Twitter o Skype la respuesta en cualquier caso será devuelta a la brevedad por el equipo de Sublime Funds.
        </div>
      </div>
      <div class="barrita_dashed_1"></div>
    </div>
  </div>
@endsection
