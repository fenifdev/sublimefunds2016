@extends('layouts.front')
@section('content')
<style>
.carousel .item {
  width: 100%;
  max-height: 300px;
}
</style>
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin:0;padding:0;">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="{!!URL::asset('img/slider.jpg')!!}" alt="..." width="100%">
        <div class="carousel-caption" style="top:0px;">
          <div style="display: table; height: 100%; width:100%; overflow: hidden;">
            <div style="display: table-cell; vertical-align: middle;">
              <div style="font-size:2em;">
                Un mundo de Oportunidades al Alcanze de su Mano
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <img src="{!!URL::asset('img/slider2.jpeg')!!}" alt="..." width="100%">
        <div class="carousel-caption" style="top:0px;">
          <div style="display: table; height: 100%; width:100%; overflow: hidden;">
            <div style="display: table-cell; vertical-align: middle;">
              <div style="font-size:2em;">
                Un mundo de Oportunidades al Alcanze de su Mano
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <img src="{!!URL::asset('img/slider3.jpeg')!!}" alt="..." width="100%">
        <div class="carousel-caption" style="top:0px;">
          <div style="display: table; height: 100%; width:100%; overflow: hidden;">
            <div style="display: table-cell; vertical-align: middle;">
              <div style="font-size:2em;">
                Un mundo de Oportunidades al Alcanze de su Mano
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <img src="{!!URL::asset('img/slider4.jpeg')!!}" alt="..." width="100%">
        <div class="carousel-caption" style="top:0px;">
          <div style="display: table; height: 100%; width:100%; overflow: hidden;">
            <div style="display: table-cell; vertical-align: middle;">
              <div style="font-size:2em;">
                Un mundo de Oportunidades al Alcanze de su Mano
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
  </div>
  <div class='container'>
    <br/>
    <h3 class="text-center color_propio_1">Sublime Funds hace que sea fácil para usted invertir como un profesional en minutos.</h3>
    <h4 class="text-center">Solo necesita crear una cuenta y comenzar a invertir!</h4>
    <br/>
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6 text-center">
        @if(Auth::user())
          <a href="{!!action('UserController@dashboard')!!}"><button class="btn btn-color_propio btn-lg btn-block">Ir al Panel</button></a>
        @else
          <a href="{!!action('Auth\AuthController@getRegister')!!}"><button class="btn btn-color_propio btn-lg btn-block">Crear Cuenta</button></a>
        @endif
      </div>
      <div class="col-md-3"></div>
    </div>
    <br/>
    <br/>
    <div class="row">
        <div class="col-md-4">
          <div class="icono-redondo">
            <i class="fa fa-money"></i>
          </div>
          <h4 class="text-center">Transparencia</h4>
          <h5 class="text-center">Seguridad y transparencia en sus transacciones. los fondos estan asegurados</h5>
        </div>
        <div class="col-md-4">
          <div class="icono-redondo">
            <i class="fa fa-bolt"></i>
          </div>
          <h4 class="text-center">
            Velocidad
          </h4>
          <h5 class="text-center">Todas las operaciones se procesan en menos de 24hs</h5>
        </div>
        <div class="col-md-4">
          <div class="icono-redondo">
            <i class="fa fa-support"></i>
          </div>
          <h4 class="text-center">
            Soporte 24/7
          </h4>
          <h5 class="text-center">Soporte via tickets, skype, twitter y facebook, usted nunca estara solo.</h5>
        </div>
    </div>
  </div>
@endsection
