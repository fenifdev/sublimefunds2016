@extends('layouts.front')
@section('content')
  <div class="container">
  	<h3>Politica de Privacidad</h3>
  </div>
  <div class="barrita_basica_1"></div>
  <br/>
  <div class="container">
  	<p class="color_propio_2">La misma se aplica a la utilización del sitio y la plataforma virtual de SUBLIME FUNDS LTD. Tal política no se aplica a sitios a los que el usuario pudiera acceder por medio de links, o cualquier otro tipo de vínculo.</p>
  	<p class="color_propio_2">Sublime Funds LTD sólo utilizará la información referida a su identidad como usuario para los fines propios del Servicio, o con fines de prestar servicios que nuestros socios comerciales brindan, pero en ningún caso revelaremos esa información a terceros.
  	El envió de mensajes que realizara SUBLIME FUNDS LTD serán exclusivamente sobre mejoras promociones y/o comunicados referidos al servicio y sus prestaciones.</p>
  	<p class="color_propio_2">La Seguridad para protección de su privacidad y datos, es una de nuestras metas. Hemos tomado todas las medidas a nuestro alcance tecnológico y económico a fin de asegurar su privacidad. Dentro de esas limitaciones extremamos las precauciones en la transmisión de la información desde su computadora hacia nuestros servers.</p>
  	<p class="color_propio_2">Ud. podrá corregir o suprimir su Información Personal archivada en nuestra base de datos. Para ello dispondrá de un área específica para tal fin en nuestro sitio.</p>
  	<p class="color_propio_2">La utilización de cualquiera de los Sitios utilizados por nuestros socios comerciales, importa su aceptación de nuestra Política de Privacidad. Si Ud. no está de acuerdo con esta política, por favor no utilice nuestros servicios. De igual forma, si Ud. continua haciendo uso de los Sitios y Servicios de nuestros socios después de que hayamos introducido cualquier modificación a esa política, ello importará su aceptación de esas modificaciones.</p>
  	<p class="color_propio_2">Recuerde que Sublime Funds LTD es un servicio de inversión online, no es una entidad bancaria. La devolución de su dinero está garantizada desde el momento de su aceptación.</p>
  </div>
@endsection
