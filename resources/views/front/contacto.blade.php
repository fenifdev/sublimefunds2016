@extends('layouts.front')
@section('content')
  <div class="container">
  	<h3>Contacto</h3>
  </div>
  <div class="barrita_basica_1"></div>
  <br/>
  <div class="container">
  	<div class="row">
  		<h2 class="text-center">Yes, let’s talk business!</h2>
  		<br/>
  		<p class="text-center">Estaríamos encantados de tener comentarios de ustedes. Envíenos una línea, si se trata de un comentario, una pregunta, una proposición de trabajo o simplemente un hola. Usted puede utilizar el siguiente formulario o los datos de contacto de la derecha.</p>
  		<br/>
      @if(count($errors)>0)
      <div class="alert alert-danger text-center">
        <ul>
          @foreach($errors->all() as $error)
          <li>{!!$error!!}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if (session('status'))
      <div class="alert alert-success text-center">
          {{ session('status') }}
      </div>
      @endif
      {!!Form::open(['route' => 'front.contacto', 'method'=>'POST'])!!}
  			<div class="col-md-4">
  				<div class="form-group">
  					<div class="left-inner-addon">
  	    				<span class="glyphicon glyphicon-user"></span>
                {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Name'])!!}
  					</div>
  				</div>
  			</div>
  			<div class="col-md-4">
  				<div class="form-group">
  					<div class="left-inner-addon">
  	    				<span class="glyphicon glyphicon-pencil"></span>
                {!!Form::text('subject',null,['class'=>'form-control','placeholder'=>'Subject'])!!}
  					</div>
  				</div>
  			</div>
  			<div class="col-md-4">
  				<div class="form-group">
  					<div class="left-inner-addon">
  	    				<span class="glyphicon glyphicon-envelope"></span>
                {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'Email'])!!}
  					</div>
  				</div>
  			</div>
  			<div class="col-md-12">
  				<div class="form-group">
  					<div class="left-inner-addon">
  	    				<span class="glyphicon glyphicon-pencil"></span>
                {!!Form::textarea('message',null,['class'=>'form-control','placeholder'=>'Message','rows'=>'4'])!!}
  					</div>
  				</div>
  			</div>
  			<div class="col-md-12">
  				<div class="form-group">
            {!!Form::submit('Send',['class'=>'btn btn-color_propio btn-block'])!!}
  				</div>
  			</div>
  		{!!Form::close()!!}
  	</div>
  </div>
@endsection
