@extends('layouts.front')
@section('content')
  <div class="container">
  	<h3>Planes</h3>
  </div>
  <div class="barrita_basica_1"></div>
  <br/>
  <div class="container">
  	<div class="row">
  		<div class="col-md-1 col-lg-1"></div>
  		<div class="col-md-2 col-lg-2">
  			<table class='table text-center'>
  				<thead>
  					<tr>
  						<th><h3 class="text-center color_propio_1">Cobre</h3></th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<td><i class="fa fa-line-chart"></i> TNM 4%</td>
  					</tr>
  					<tr>
  						<td><i class="fa fa-clock-o"></i> 1 Mes</td>
  					</tr>
  					<tr>
  						<td><i class="fa fa-support"></i> 24/7 Support</td>
  					</tr>
  					<tr>
  						<td><a href="{!!$link!!}"><button class="btn btn-color_propio">Empezar Ahora!</button></a></td>
  					</tr>
  				</tbody>
  			</table>
  		</div>
  		<div class="col-md-2 col-lg-2">
  			<table class='table text-center'>
  				<thead>
  					<tr>
  						<th><h3 class="text-center color_propio_1">Bronze</h3></th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<td><i class="fa fa-line-chart"></i> TNM 5%</td>
  					</tr>
  					<tr>
  						<td><i class="fa fa-clock-o"></i> 3 Meses</td>
  					</tr>
  					<tr>
  						<td><i class="fa fa-support"></i> 24/7 Support</td>
  					</tr>
  					<tr>
  						<td><a href="{!!$link!!}"><button class="btn btn-color_propio">Empezar Ahora!</button></a></td>
  					</tr>
  				</tbody>
  			</table>
  		</div>
  		<div class="col-md-2 col-lg-2">
  			<table class='table text-center'>
  				<thead>
  					<tr>
  						<th><h3 class="text-center color_propio_1">Plata</h3></th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<td><i class="fa fa-line-chart"></i> TNM 6%</td>
  					</tr>
  					<tr>
  						<td><i class="fa fa-clock-o"></i> 6 Meses</td>
  					</tr>
  					<tr>
  						<td><i class="fa fa-support"></i> 24/7 Support</td>
  					</tr>
  					<tr>
  						<td><a href="{!!$link!!}"><button class="btn btn-color_propio">Empezar Ahora!</button></a></td>
  					</tr>
  				</tbody>
  			</table>
  		</div>
  		<div class="col-md-2 col-lg-2">
  			<table class='table text-center'>
  				<thead>
  					<tr>
  						<th><h3 class="text-center color_propio_1">Oro</h3></th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<td><i class="fa fa-line-chart"></i> TNM 7%</td>
  					</tr>
  					<tr>
  						<td><i class="fa fa-clock-o"></i> 9 Meses</td>
  					</tr>
  					<tr>
  						<td><i class="fa fa-support"></i> 24/7 Support</td>
  					</tr>
  					<tr>
  						<td><a href="{!!$link!!}"><button class="btn btn-color_propio">Empezar Ahora!</button></a></td>
  					</tr>
  				</tbody>
  			</table>
  		</div>
  		<div class="col-md-2 col-lg-2">
  			<table class='table text-center'>
  				<thead>
  					<tr>
  						<th><h3 class="text-center color_propio_1">Diamante</h3></th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<td><i class="fa fa-line-chart"></i> TNM 8%</td>
  					</tr>
  					<tr>
  						<td><i class="fa fa-clock-o"></i> 12 Meses</td>
  					</tr>
  					<tr>
  						<td><i class="fa fa-support"></i> 24/7 Support</td>
  					</tr>
  					<tr>
  						<td><a href="{!!$link!!}"><button class="btn btn-color_propio">Empezar Ahora!</button></a></td>
  					</tr>
  				</tbody>
  			</table>
  		</div>
  		<div class="col-md-1 col-lg-1"></div>
  	</div>
  	<br/>
  	<h2 class="text-center color_propio_1">Todos nuestros planes incluyen</h2>
  	<br/>
  	<div class="row">
  		<div class="col-md-2 col-lg-2">
  			<div class="icono-redondo">
  	        	<i class="fa fa-money"></i>
  	       	</div>
  	       	<h4 class="text-center">Transparencia</h4>
  	        <h5 class="text-center">Seguridad y transparencia en sus transacciones. los fondos estan asegurados</h5>
  	    </div>
  		<div class="col-md-2 col-lg-2">
  			<div class="icono-redondo">
  	        	<i class="fa fa-bolt"></i>
  	        </div>
  	        <h4 class="text-center">
  	        	Velocidad
  	        </h4>
  	        <h5 class="text-center">Todas las operaciones se procesan en menos de 24hs</h5>
  		</div>
  		<div class="col-md-2 col-lg-2">
  			<div class="icono-redondo">
  	        	<i class="fa fa-support"></i>
  	        </div>
  	        <h4 class="text-center">
  	        	Soporte 24/7
  	        </h4>
  	        <h5 class="text-center">Soporte via tickets, skype, twitter y facebook, usted nunca estara solo.</h5>
  		</div>
  		<div class="col-md-2 col-lg-2">
  			<div class="icono-redondo">
  	        	<i class="fa fa-thumbs-o-up"></i>
  	        </div>
  	        <h4 class="text-center">
  	        	Sin Comisiones
  	        </h4>
  	        <h5 class="text-center">Nos hacemos cargo de las comisiones de retiros y depósitos.</h5>
  		</div>
  		<div class="col-md-2 col-lg-2">
  			<div class="icono-redondo">
  	        	<i class="fa fa-thumbs-o-up"></i>
  	        </div>
  	        <h4 class="text-center">
  	        	Seguridad
  	        </h4>
  	        <h5 class="text-center">Nos hacemos cargo de las comisiones de retiros y depósitos.</h5>
  		</div>
  		<div class="col-md-2 col-lg-2">
  			<div class="icono-redondo">
  	        	<i class="fa fa-thumbs-o-up"></i>
  	        </div>
  	        <h4 class="text-center">
  	        	Seguridad
  	        </h4>
  	        <h5 class="text-center">Nos hacemos cargo de las comisiones de retiros y depósitos.</h5>
  		</div>
  	</div>
  </div>
@endsection
