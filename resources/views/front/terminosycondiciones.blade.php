@extends('layouts.front')
@section('content')
  <div class="container">
  	<h3>Terminos y Condiciones</h3>
  </div>
  <div class="barrita_basica_1"></div>
  <br/>
  <div class="container">
  	<ol>
  		<li>
  		  	<p class="color_propio_2">El Usuario tiene derecho a participar en el programa de inversión sólo si es mayor de edad según las leyes de la jurisdicción de origen. <br/>
  			Sublime Funds LTD se compromete a guardar todos los datos proporcionados por el usuario y no compartirlos con terceros. La cuenta creada es de uso exclusivo y personal, cualquier acción fuera del contexto de SublimeFunds.com por parte del usuario es pura y exclusivamente responsabilidad del mismo.<br/>
  			En el formulario de inscripción y datos del perfil el usuario se compromete a colocar información veraz y de fácil comprobación, si así lo requiere Sublime Funds LTD. De lo contrario, la administración tiene el pleno derecho a suspender la cuenta y congelar los fondos hasta que la situación sea aclarada por ambas partes.</p>
  		</li>
  		<li>
  			<p class="color_propio_2">El Usuario puede utilizar la plataforma de inversión suministrada por Sublime Funds LTD para inversiones con capital propio y/o de terceros, bajo su propia responsabilidad. <br/>
  			El Usuario se compromete a llevar una conducta de moral y respeto hacia el equipo de Sublime Funds LTD. En el caso de no hacerlo Sublime Funds LTD se reserva el derecho de admisión. <br/>
  			Si el equipo de Sublime Funds LTD detecta cualquier acción fraudulenta relacionada con las actividades del Usuario, se restringirá el  acceso a la cuenta personal y sin explicación. En este caso, se comunicara los por medio de los canales habilitados el motivo y causa de dicha acción, sin tener el Usuario derecho a solicitar cualquier tipo de reembolso o liquidación.</p>
  		</li>
  		<li>
  			<p class="color_propio_2">Sublime Funds LTD se compromete a tener los estándares de seguridad para la protección y bloqueo de accesos no permitidos al sitio y área de usuarios.</p>
  		</li>
  		<li>
  			<p class="color_propio_2">El usuario debe realizar la inversión mínima para poder tener su cuenta activa, de lo contrario su cuenta será borrada en un lapso de 60 días corridos.</p>
  		</li>
  		<li>
  			<p class="color_propio_2">Los medios de inversión y pagos son únicamente los utilizados en www.sublimefunds.com .</p>
  		</li>
  		<li>
  			<p class="color_propio_2">Todos los canales de consultas estarán activos y abiertos para que el Usuario pueda realizar sus consultas y/o reclamos de forma rápida y efectiva.</p>
  		</li>
  		<li>
  			<p class="color_propio_2">Sublime Funds LTD se compromete a tomar el capital invertido y devolverlo íntegramente mas las comisiones generadas dependiendo el tipo de plan y periodo elegido por el usuario al momento de realizar su inversión.<br/>
  			Al  aceptar los términos y condiciones el usuario, comprende los riesgos sobre los sistemas de inversión, Sublime Funds LTD se compromete a NO tomar inversiones que a un futuro sean incumplidas, por ello avisara por todos los medios disponibles sobre la situación actual.</p>
  		</li>
  		<li>
  			<p class="color_propio_2">Para solicitar retiro de fondos, el usuario debe invertir primero la cantidad mínima.</p>
  		</li>
  	</ol>
  </div>
@endsection
