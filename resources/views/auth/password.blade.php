@extends('layouts.front')
@section('content')
  <div class="container">
  	<h3>Reset Password</h3>
  </div>
  <div class="barrita_basica_1"></div>
  <br/>
  <div class="container">
    @if(count($errors)>0)
    <div class="alert alert-danger text-center">
      <ul>
        @foreach($errors->all() as $error)
        <li>{!!$error!!}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="row">
      <div class="col-lg-3 col-md-3"></div>
      <div class="col-lg-6 col-md-6">
        {!!Form::open(['url' => '/password/email', 'method'=>'POST', 'class'=>'form-horizontal', 'role'=>'form'])!!}
          <div class="form-group">
            <label for="exampleInputEmail1" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'Email','value'=>"{{ old('email') }}"])!!}
            </div>
          </div>

          <div class="col-sm-2"></div>
          {!!Form::submit('Send Password Reset Link',['class'=>'btn btn-color_propio pull-right'])!!}
        {!!Form::close()!!}
      </div>
      <div class="col-lg-3 col-md-3"></div>
    </div>
  </div>
@endsection
