@extends('layouts.front')
@section('content')
  <div class="container">
  	<h3>Reset Password</h3>
  </div>
  <div class="barrita_basica_1"></div>
  <br/>
  <div class="container">
    @if(count($errors)>0)
    <div class="alert alert-danger text-center">
      <ul>
        @foreach($errors->all() as $error)
        <li>{!!$error!!}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="row">
      <div class="col-lg-3 col-md-3"></div>
      <div class="col-lg-6 col-md-6">
        {!!Form::open(['url' => '/password/reset', 'method'=>'POST', 'class'=>'form-horizontal', 'role'=>'form'])!!}
          {!!Form::hidden('token', $token, null)!!}
          <div class="form-group">
            <label for="exampleInputEmail1" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'Email','value'=>"{{ old('email') }}"])!!}
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
              {!!Form::password('password',['class'=>'form-control','placeholder'=>'Contraseña'])!!}
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1" class="col-sm-2 control-label">Confirm Password</label>
            <div class="col-sm-10">
              {!!Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Contraseña'])!!}
            </div>
          </div>

          <div class="col-sm-2"></div>
          {!!Form::submit('Reset Password',['class'=>'btn btn-color_propio pull-right'])!!}
        {!!Form::close()!!}
      </div>
      <div class="col-lg-3 col-md-3"></div>
    </div>
  </div>
</form>
@endsection
