@extends('layouts.front')
@section('content')
  <div class="container">
  	<h3>Login</h3>
  </div>
  <div class="barrita_basica_1"></div>
  <br/>
  <div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning">
            {{ session('warning') }}
        </div>
    @endif
    @if(count($errors)>0)
    <div class="alert alert-danger text-center">
      <ul>
        @foreach($errors->all() as $error)
        <li>{!!$error!!}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="row">
      <div class="col-lg-3 col-md-3"></div>
      <div class="col-lg-6 col-md-6">
        {!!Form::open(['url' => 'login', 'method'=>'POST', 'class'=>'form-horizontal', 'role'=>'form'])!!}
          <div class="form-group">
            <label for="exampleInputEmail1" class="col-sm-2 control-label">Username</label>
            <div class="col-sm-10">
              {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'Email'])!!}
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
              {!!Form::password('password',['class'=>'form-control','placeholder'=>'Contraseña'])!!}
            </div>
          </div>
          <div class="col-sm-2"></div>
          {!!link_to('/password/email', $title = 'Lost your password?', $attributes = ['class'=>'color_propio_1'], $secure = null)!!}
          {!!Form::submit('Login',['class'=>'btn btn-color_propio pull-right'])!!}
        {!!Form::close()!!}
      </div>
      <div class="col-lg-3 col-md-3"></div>
    </div>
  </div>
@endsection
