@extends('layouts.front')
@section('content')
  <div class="container">
  	<h3>Registro</h3>
  </div>
  <div class="barrita_basica_1"></div>
  <br/>
  <div class="container">
  	<div class="row">
      @if(count($errors)>0)
      <div class="alert alert-danger text-center">
        <ul>
          @foreach($errors->all() as $error)
          <li>{!!$error!!}</li>
          @endforeach
        </ul>
      </div>
      @endif
  		<div class="col-lg-3 col-md-3"></div>
  		<div class="col-lg-6 col-md-6">
        {!!Form::open(['url' => 'register', 'method'=>'POST', 'class'=>'form-horizontal', 'role'=>'form'])!!}
  				<div class="form-group">
  					<label for="exampleInputName" class="control-label col-lg-4 col-md-4">Nombre:</label>
  					<div class="col-lg-8 col-md-8">
              {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Nombre'])!!}
  					</div>
  				</div>
  				<div class="form-group">
  					<label for="exampleInputEmail" class="control-label col-lg-4 col-md-4">Correo:</label>
  					<div class="col-lg-8 col-md-8">
              {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'Email'])!!}
  					</div>
  				</div>
  				<div class="form-group">
  					<label for="exampleInputPassword" class="control-label col-lg-4 col-md-4">Contraseña:</label>
  					<div class="col-lg-8 col-md-8">
              {!!Form::password('password',['class'=>'form-control','placeholder'=>'Contraseña'])!!}
  					</div>
  				</div>
  				<div class="form-group">
  					<label for="exampleInputRePassword" class="control-label col-lg-4 col-md-4">Re-Contraseña:</label>
  					<div class="col-lg-8 col-md-8">
              {!!Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Confirmar Contraseña'])!!}
  					</div>
  				</div>
  				<div class="text-right">
            {!!Form::submit('Registrar',['class'=>'btn btn-color_propio'])!!}
  				</div>
  			{!!Form::close()!!}
  		</div>
  		<div class="col-lg-3 col-md-3"></div>
  	</div>
  </div>
@endsection
