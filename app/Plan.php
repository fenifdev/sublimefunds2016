<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'plans';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','tnm','months'];

    /**
     * Get the investments record associated with the plan.
     */
    public function investments()
    {
        return $this->hasMany('App\Investment');
    }
}
