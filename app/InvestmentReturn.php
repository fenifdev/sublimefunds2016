<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestmentReturn extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'investments_returns';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['investment_id','amount','date_end','status'];

    /**
     * Get the investment that owns the returns.
     */
    public function investment()
    {
        return $this->belongsTo('App\Investment');
    }
}
