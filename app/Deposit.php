<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'deposits';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','amount','ref_paypal','status'];

    /**
     * Get the user that owns the deposit.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
