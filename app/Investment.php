<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'investments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','plan_id','amount','date_start','date_end','status'];

    /**
     * Get the user that owns the investments.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the plan that owns the investments.
     */
    public function plan()
    {
        return $this->belongsTo('App\Plan');
    }

    /**
     * Get the returns record associated with the investment.
     */
    public function InvestmentsReturns()
    {
        return $this->hasMany('App\InvestmentReturn');
    }
}
