<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','transaction_type_id','id_ref','amount','ref_paypal','status'];

    /**
     * Get the TransactionType record associated with the Transaction.
     */
    public function transactionType()
    {
        return $this->belongsTo('App\TransactionType');
    }

    /**
     * Get the user that owns the withdrawal.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
