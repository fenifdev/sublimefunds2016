<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Withdrawal;
use App\Transaction;
use App\Wallet;
use Auth;
use DB;
use Validator;

class WithdrawalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::findOrFail(Auth::user()->id);
        $user->wallet->balance_available = number_format($user->wallet->balance - $user->wallet->invested - $user->wallet->pending_withdrawal,2);
        return view('withdrawal.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //Calculate balance available for Withdrawal
      $user = User::findOrFail(Auth::user()->id);
      $user->wallet->balance_available = $user->wallet->balance - $user->wallet->invested - $user->wallet->pending_withdrawal;
        $validator = Validator::make($request->all(), [
           'amount' => 'required|numeric|min:100|max:'.$user->wallet->balance_available,
       ]);

       if ($validator->fails()) {
           return redirect('/panel/create_withdrawal')
                       ->withErrors($validator)
                       ->withInput();
       }
      $data = $request->all();
      $withdrawal = DB::transaction(function () use($data) {
          //Crear el deposito.
          $withdrawal = Withdrawal::create([
              'user_id' => Auth::user()->id,
              'amount' => $data['amount'],
              'status' => 0,
          ]);
          //Creo la transacción
          Transaction::create([
              'user_id' => Auth::user()->id,
              'transaction_type_id' => 2,
              'id_ref' => $withdrawal->id,
              'amount' => $data['amount'],
              'status' => 0,
          ]);
          //Sumo a la billetera
          $wallet = Wallet::find(Auth::user()->id);
          $wallet->balance = $wallet->balance-$data['amount'];
          $wallet->save();

          return $withdrawal;
      });
      return redirect('/panel/create_withdrawal')->with('status', 'Withdrawal Store!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
