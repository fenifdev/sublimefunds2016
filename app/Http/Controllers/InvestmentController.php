<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Plan;
use App\Investment;
use App\InvestmentReturn;
use App\Transaction;
use App\Wallet;
use Auth;
use DB;
use Validator;
use Yajra\Datatables\Datatables;

class InvestmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);
        return view('investment.index')->with(['user' => $user]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAdmin()
    {
        $investments = Investment::All();
        return view('investment.indexAdmin')->with(['investments' => $investments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::findOrFail(Auth::user()->id);
        $user->wallet->balance_available = number_format($user->wallet->balance - $user->wallet->invested - $user->wallet->pending_withdrawal,2);
        $data_plans = Plan::lists('name','id');
        return view('investment.create')->with(['user' => $user,'data_plans' => $data_plans]);
        #return view('investment.create',compact('data_plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Calculate balance available for investment
        $user = User::findOrFail(Auth::user()->id);
        $user->wallet->balance_available = $user->wallet->balance - $user->wallet->invested - $user->wallet->pending_withdrawal;
        $validator = Validator::make($request->all(), [
             'amount' => 'required|numeric|min:100|max:'.$user->wallet->balance_available,
             'plan_id' => 'required|exists:plans,id',
        ]);

        if ($validator->fails()) {
            return redirect('/panel/create_investment')
                          ->withErrors($validator)
                          ->withInput();
        }
        #dd($request->all());
        $data = $request->all();
        $investment = DB::transaction(function () use($data) {
            //Obtengo data del plan
            $plan = Plan::find($data['plan_id']);
            //CAlculo fecha y hora
						$fecha = date('Y-m-d');
						$hora = date('H:i:s');
						//Calculo fecha inicio
						$fecha_inicio = date('Y-m-d', strtotime('+1 day', strtotime($fecha)));
            //Calculo fecha fin
						$fecha_fin = date('Y-m-d', strtotime('+'.$plan->months.' month', strtotime($fecha_inicio)));
            //Crear la inversion.
            $investment = Investment::create([
                'user_id' => Auth::user()->id,
                'plan_id' => $data['plan_id'],
                'amount' => $data['amount'],
                'date_start' => $fecha_inicio,
                'date_end' => $fecha_fin,
                'status' => 0,
            ]);

            //Creo la transacción
            Transaction::create([
                'user_id' => Auth::user()->id,
                'transaction_type_id' => 3,
                'id_ref' => $investment->id,
                'amount' => $data['amount'],
                'status' => 0,
            ]);
            //Sumo a monto invertido la billetera
            $wallet = Wallet::find(Auth::user()->id);
            $wallet->invested = $wallet->invested+$data['amount'];
            $wallet->save();

            //Calculo el monto del retorno
						$amount_return = number_format($data['amount']*$plan->tnm/100 ,2);
            for($i=0;$i<$plan->months;$i++){
                $x=$i+1;
								$fecha_fin_retorno = date('Y-m-d', strtotime('+'.$x.' month', strtotime($fecha_inicio)));
                InvestmentReturn::create([
                    'investment_id' => $investment->id,
                    'amount' => $amount_return,
                    'date_end' => $fecha_fin_retorno,
                    'status' => 0,
                ]);
            }

            return $investment;
        });
        return redirect('/panel/create_investment')->with('status', 'Investment Store!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function datatablesData()
    {
        $investments = Investment::where('user_id',Auth::user()->id);
        #return Datatables::of($investments)->make(true);
        return Datatables::of($investments)->addColumn('plan_name', function ($investment) {
                      return $investment->Plan->name;
                  })
                  ->make(true);
    }

    public function datatablesDataAdmin()
    {
        $investments = Investment::All();
        #return Datatables::of($investments)->make(true);
        return Datatables::of($investments)->addColumn('plan_name', function ($investment) {
                      return $investment->Plan->name;
                  })
                  ->make(true);
    }
}
