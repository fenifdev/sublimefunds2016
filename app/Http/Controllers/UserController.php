<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Yajra\Datatables\Datatables;
use Form;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display dashboard for user.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $user = User::findOrFail(Auth::user()->id);
        $user->wallet->balance_available = number_format($user->wallet->balance - $user->wallet->invested - $user->wallet->pending_withdrawal,2);
        return view('user.dashboard',compact('user'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('user.index')->with(['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $data_role = ['user'=>'user','admin'=>'admin'];
        return view('user.edit')->with(['user' => $user,'data_role' => $data_role]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();
        return redirect('/admin/user')->with('status', 'User Save!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/admin/user')->with('status', 'User Delete!');
    }

    public function datatablesData()
    {
        $users = User::select(['id', 'name', 'email', 'created_at', 'updated_at']);
        return Datatables::of($users)->addColumn('action', function ($user) {
                $algo = link_to_route('admin.user.edit', $title = 'Editar', $parameters = $user->id, $attributes = ['class'=>'btn btn-primary btn-xs']);
                $algo .= Form::open(['route' => ['admin.user.destroy',$user->id] ,'method'=>'DELETE', 'style'=>'display:inline;padding:0 0 0 10px;']);
                $algo .= Form::submit('Eliminar',['class'=>'btn btn-danger btn-xs']);
                $algo .= Form::close();
                return $algo;
            })
            ->make(true);
    }
}
