<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ContactoRequest;

use Mail;
use Auth;

class FrontController extends Controller
{
    public function getIndex()
    {
        return view('front.index');
    }

    public function getNosotros()
    {
        return view('front.nosotros');
    }

    public function getPlanes()
    {
        if(Auth::user()){
          $link = action('UserController@dashboard');
        }else{
          $link = action('Auth\AuthController@getRegister');
        }

        return view('front.planes',compact('link'));
    }

    public function getFaq()
    {
        return view('front.faq');
    }

    public function getContacto()
    {
        return view('front.contacto');
    }

    public function postContacto(ContactoRequest $request)
    {
        $data = $request->all();
        $data['messages'] = $data['message'];
        /*
        $message = sprintf('Name: %s <br/>Email: %s <br/> Subject: %s <br/> Message: %s', $data['name'], $data['email'], $data['subject'], $data['message']);
        Mail::raw($message, function ($m) use ($data) {
            $m->to('flores.federico.nicolas@gmail.com', 'Admin')
              ->from('admin@fenifdev.ga', 'SublimeFunds')
              ->subject('Contacto');
        });
        */
        Mail::send('emails.contact', $data, function ($message) {
            $message->from('admin@fenifdev.ga', 'Admin');
            $message->subject('Contacto');
            $message->to('flores.federico.nicolas@gmail.com');
        });
        return redirect()->route('front.contacto')->with('status', 'El mensaje ha sido enviado con exito.');
    }

    public function getPoliticadeprivacidad()
    {
        return view('front.politicadeprivacidad');
    }

    public function getTerminosycondiciones()
    {
        return view('front.terminosycondiciones');
    }

}
