<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Deposit;
use App\Transaction;
use App\Wallet;
use App\User;
use DB;
use Srmklive\PayPal\Services\ExpressCheckout;
use Session;
use Uuid;
use App\Http\Requests\StoreDepositRequest;

class DepositController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('deposit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepositRequest $request)
    {
        $data_post = $request->all();
        $provider = new ExpressCheckout;      // To use express checkout.
        $data = [];
        $data['items'] = [
            [
                'name' => 'Deposit',
                'price' => $data_post['amount'],
                'qty' => 1
            ]
        ];
        #$data['invoice_id'] = 'pago1231432sdf3';
        $data['invoice_id'] = Uuid::generate();
        $data['invoice_description'] = "Order #$data[invoice_id] Invoice";
        $data['return_url'] = url('/panel/proccess_deposit');
        $data['cancel_url'] = url('/panel/proccess_deposit');
        $total = 0;
        foreach($data['items'] as $item) {
            $total += $item['price'];
        }
        $data['total'] = $total;
        #dd($data);
        $response = $provider->setExpressCheckout($data);
        // Here you store it in session with session helper, with the key as transaction token.
        session()->put($response['TOKEN'], $data);
        #dd($response);

        // This will redirect user to PayPal
        return redirect($response['paypal_link']);
        /*
        $data = $request->all();
        $deposit = DB::transaction(function () use($data) {
            //Crear el deposito.
            $deposit = Deposit::create([
                'user_id' => Auth::user()->id,
                'amount' => $data['amount'],
                'status' => 1,
            ]);
            //Creo la transacción
            Transaction::create([
                'user_id' => Auth::user()->id,
                'transaction_type_id' => 1,
                'id_ref' => $deposit->id,
                'amount' => $data['amount'],
                'status' => 1,
            ]);
            //Sumo a la billetera
            $wallet = Wallet::find(Auth::user()->id);
            $wallet->balance = $wallet->balance+$data['amount'];
            $wallet->save();

            return $deposit;
        });
        return redirect('/panel/create_deposit')->with('status', 'Deposit Store!');
        */

    }

    public function proccess()
    {
        $token = \Input::get('token');
        $PayerID = \Input::get('PayerID');
        /*if (empty($token) && empty($PayerID)){
            #dd('PayerID vacio');
            $paypal_error = 'El pago no ha sido procesado';
        }
        if (empty(session($token))){
            #dd('Data vacio');
            $paypal_error = 'Data vacio';
        }*/
        if (empty($token) || empty($PayerID) || empty(session($token))){
            #Session::flash('paypal_error', 'El pago no ha sido procesado');
            return redirect('/panel/create_deposit')->with('paypal_error', 'El pago no ha sido procesado');
        }else{
            #dd($token);
            $provider = new ExpressCheckout;      // To use express checkout.
            #$response = $provider->getExpressCheckoutDetails($token);
            $response = $provider->doExpressCheckoutPayment(session($token), $token, $PayerID);
            #dd($response);
            if($response['PAYMENTINFO_0_PAYMENTSTATUS']=='Completed'){
                /*Valido si el pago ya existe*/
                $deposit = Deposit::where('ref_paypal',$response['PAYMENTINFO_0_TRANSACTIONID'])->first();
                if(count($deposit)>0){
                    return redirect('/panel/create_deposit')->with('status', 'Pago fue cargado con anterioridad.');
                }
                /*Create deposit*/
                $deposit = DB::transaction(function () use($response) {
                    //Crear el deposito.
                    $deposit = Deposit::create([
                        'user_id' => Auth::user()->id,
                        'amount' => $response['PAYMENTINFO_0_AMT'],
                        'ref_paypal' => $response['PAYMENTINFO_0_TRANSACTIONID'],
                        'status' => 1,
                    ]);
                    //Creo la transacción
                    Transaction::create([
                        'user_id' => Auth::user()->id,
                        'transaction_type_id' => 1,
                        'id_ref' => $deposit->id,
                        'amount' => $response['PAYMENTINFO_0_AMT'],
                        'ref_paypal' => $response['PAYMENTINFO_0_TRANSACTIONID'],
                        'status' => 1,
                    ]);
                    //Sumo a la billetera
                    $wallet = Wallet::find(Auth::user()->id);
                    $wallet->balance = $wallet->balance+$response['PAYMENTINFO_0_AMT'];
                    $wallet->save();

                    return $deposit;
                });
                /*****************/
                return redirect('/panel/create_deposit')->with('status', 'Pago realizado correctamente');
            }
            #return redirect('/panel/create_deposit')->with('paypal_error', 'El pago no ha sido procesado');
            dd($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
