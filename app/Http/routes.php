<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@postRegister');

Route::get('user/activation/{confirmation_code}', 'Auth\AuthController@activateUser')->name('user.activate');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['prefix' => 'panel'], function () {
  Route::get('/', 'UserController@dashboard');
  Route::get('/create_deposit', 'DepositController@create');
  Route::post('/create_deposit', 'DepositController@store');
  Route::get('/proccess_deposit', 'DepositController@proccess');
  Route::get('/create_withdrawal', 'WithdrawalController@create');
  Route::post('/create_withdrawal', 'WithdrawalController@store');
  Route::get('/create_investment', 'InvestmentController@create');
  Route::post('/create_investment', 'InvestmentController@store');
  Route::get('/investment', 'InvestmentController@index');
  Route::get('/investmentDatatablesData', 'InvestmentController@datatablesData');
  Route::get('/transaction', 'TransactionController@index');
  Route::get('/transactionDatatablesData', 'TransactionController@datatablesData');
});

Route::group(['prefix' => 'admin'], function () {
  Route::get('user/datatablesData', 'UserController@datatablesData');
  Route::resource('user','UserController');
  Route::get('/investment', 'InvestmentController@indexAdmin');
  Route::get('/investmentDatatablesData', 'InvestmentController@datatablesDataAdmin');
  Route::get('/transaction', 'TransactionController@indexAdmin');
  Route::get('/transactionDatatablesData', 'TransactionController@datatablesDataAdmin');
});

Route::controller('/', 'FrontController', [
    'getIndex' => 'front.index',
    'getNosotros' => 'front.nosotros',
    'getPlanes' => 'front.planes',
    'getFaq' => 'front.faq',
    'getContacto' => 'front.contacto',
    'postContacto' => 'front.contacto',
    'getPoliticadeprivacidad' => 'front.PoliticaDePrivacidad',
    'getTerminosycondiciones' => 'front.TerminosYCondiciones',
]);
